<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $product_name;

    /**
     * @ORM\Column(type="integer")
     */
    private $product_type;

    /**
     * @ORM\Column(type="integer")
     */
    private $product_storage;

    /**
     * @ORM\Column(type="integer")
     */
    private $product_series;

    /**
     * @ORM\Column(type="bigint")
     */
    private $product_sku;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProductName(): ?string
    {
        return $this->product_name;
    }

    public function setProductName(string $product_name): self
    {
        $this->product_name = $product_name;

        return $this;
    }

    public function getProductType(): ?int
    {
        return $this->product_type;
    }

    public function setProductType(int $product_type): self
    {
        $this->product_type = $product_type;

        return $this;
    }

    public function getProductStorage(): ?int
    {
        return $this->product_storage;
    }

    public function setProductStorage(int $product_storage): self
    {
        $this->product_storage = $product_storage;

        return $this;
    }

    public function getProductSeries(): ?int
    {
        return $this->product_series;
    }

    public function setProductSeries(int $product_series): self
    {
        $this->product_series = $product_series;

        return $this;
    }

    public function getProductSku(): ?int
    {
        return $this->product_sku;
    }

    public function setProductSku(int $product_sku): self
    {
        $this->product_sku = $product_sku;

        return $this;
    }
}
