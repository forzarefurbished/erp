<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DeviceRepository")
 */
class Device
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="bigint")
     */
    private $device_IMEI;

    /**
     * @ORM\Column(type="integer")
     */
    private $device_model;

    /**
     * @ORM\Column(type="integer")
     */
    private $device_type;

    /**
     * @ORM\Column(type="integer")
     */
    private $device_storage;

    /**
     * @ORM\Column(type="integer")
     */
    private $device_connection;

    /**
     * @ORM\Column(type="integer")
     */
    private $device_colour;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $purchase_date;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $sale_date;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDeviceIMEI(): ?int
    {
        return $this->device_IMEI;
    }

    public function setDeviceIMEI(int $device_IMEI): self
    {
        $this->device_IMEI = $device_IMEI;

        return $this;
    }

    public function getDeviceModel(): ?int
    {
        return $this->device_model;
    }

    public function setDeviceModel(int $device_model): self
    {
        $this->device_model = $device_model;

        return $this;
    }

    public function getDeviceType(): ?int
    {
        return $this->device_type;
    }

    public function setDeviceType(int $device_type): self
    {
        $this->device_type = $device_type;

        return $this;
    }

    public function getDeviceStorage(): ?int
    {
        return $this->device_storage;
    }

    public function setDeviceStorage(int $device_storage): self
    {
        $this->device_storage = $device_storage;

        return $this;
    }

    public function getDeviceConnection(): ?int
    {
        return $this->device_connection;
    }

    public function setDeviceConnection(int $device_connection): self
    {
        $this->device_connection = $device_connection;

        return $this;
    }

    public function getDeviceColour(): ?int
    {
        return $this->device_colour;
    }

    public function setDeviceColour(int $device_colour): self
    {
        $this->device_colour = $device_colour;

        return $this;
    }

    public function getPurchaseDate(): ?\DateTimeInterface
    {
        return $this->purchase_date;
    }

    public function setPurchaseDate(?\DateTimeInterface $purchase_date): self
    {
        $this->purchase_date = $purchase_date;

        return $this;
    }

    public function getSaleDate(): ?\DateTimeInterface
    {
        return $this->sale_date;
    }

    public function setSaleDate(?\DateTimeInterface $sale_date): self
    {
        $this->sale_date = $sale_date;

        return $this;
    }
}
