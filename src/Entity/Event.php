<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EventRepository")
 */
class Event
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $event_type;

    /**
     * @ORM\Column(type="integer")
     */
    private $user_id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $event_start;

    /**
     * @ORM\Column(type="datetime")
     */
    private $event_end;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $event_status;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEventType(): ?int
    {
        return $this->event_type;
    }

    public function setEventType(int $event_type): self
    {
        $this->event_type = $event_type;

        return $this;
    }

    public function getUserId(): ?int
    {
        return $this->user_id;
    }

    public function setUserId(int $user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getEventStart(): ?\DateTimeInterface
    {
        return $this->event_start;
    }

    public function setEventStart(\DateTimeInterface $event_start): self
    {
        $this->event_start = $event_start;

        return $this;
    }

    public function getEventEnd(): ?\DateTimeInterface
    {
        return $this->event_end;
    }

    public function setEventEnd(\DateTimeInterface $event_end): self
    {
        $this->event_end = $event_end;

        return $this;
    }

    public function getEventStatus(): ?int
    {
        return $this->event_status;
    }

    public function setEventStatus(?int $event_status): self
    {
        $this->event_status = $event_status;

        return $this;
    }
}
