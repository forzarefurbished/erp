<?php
/**
 * Created by PhpStorm.
 * UserController: simona
 * Date: 20/01/2019
 * Time: 11:32
 */

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Response;
use App\Model\RebuyModel;
use App\Model\DeviceModel;
use App\Model\EventModel;
use App\Model\OrderModel;


class RebuyController extends AbstractController
{
    /**
     * @Route("/rebuy/index")
     */
    public function index()
    {


        $results=RebuyModel::getStatus();
        $html=$this->render('Rebuy/index.html.twig', [

            'results'=> $results
        ]
    );
        Return new Response($html);

    }

    public function activate()
    {
        //$id = $this->route_params['id'];
        //return $id;
    }
    /**
     * @Route("/rebuy/latestorders")
     */
    public function latestorders()
    {
        $results = RebuyModel::getOrders();
        $html=$this->render('Rebuy/latestorders.html.twig'
            , [
            'results' => $results,

        ]
    );
        Return new Response($html);
    }

    /**
     * @Route("/rebuy/inspection")
     */
    public function inspection()
    {
        $results = RebuyModel::getInspection();
        $html=$this->render('Rebuy/inspection.html.twig'
        , [
            'results' => $results
        ]
         );
       Return new Response($html);

    }

    /**
     * @Route("/rebuy/{order_id}/inspect")
     */
    public function inspect($order_id)
    {
        //$order_id=$this->activate();

       $results = RebuyModel::getDevice($order_id);
        $html=$this->render('Rebuy/inspect.html.twig'
            , [
            'results' => $results,
                'order_id'=>$order_id
            ]
        );
        $_SESSION['order_id']=$order_id;
        Return new Response($html);

    }

    /**
     * @Route("/rebuy/edit")
     */
    public function edit()
    {
        $results = RebuyModel::getOrders();
        $html=$this->render('Rebuy/edit.html.twig'
            , [
            'results' => $results
        ]
    );
        Return new Response($html);
    }

    /**
     * @Route("/rebuy/mail")
     */
    public function mail()
    {

        $results = RebuyModel::getQuoted();
                $html=$this->render('Rebuy/mail.html.twig'
            , [
            'results' => $results

        ]
    );
        //use php mailer script in rebuy/mail.php
        Return new Response($html);

    }

    /**
     * @Route("/rebuy/{order_id}/sendmail")
     */
    public function sendmail($order_id)
    {
        //$order_id=$this->activate();
        $results = RebuyModel::getQuote($order_id);
        $html=$this->render('Rebuy/sendmail.html.twig'
            , [
            'results' => $results,
                'order_id'=>$order_id
       ]
        );

        Return new Response($html);
    }

    /**
     * @Route("/rebuy/{order_id}/inspectsubmit")
     */

    public function inspectsubmit($order_id)

    {
        //$order_id=$_SESSION['order_id'];

        //$order_id=$this->activateAction();


        $results = RebuyModel::getInspectionById($order_id);
        $html=$this->render('Rebuy/inspectsubmit.html.twig'
            ,[
                'results'=>$results,
                'order_id'=>$order_id

            ]
        );




        $IMEI=RebuyModel::getIMEI($order_id);
        $date = date('Y-m-d');
        $devicetype = $_POST['device_type'];
        $devicestorage = $_POST['device_storage'];
        $deviceconnection = $_POST['device_connection'];
        $devicecondition = $_POST['device_condition'];
        $devicecolour = $_POST['device_colour'];
        //$images=$_POST['images'];
        $devicecomments = $_POST['device_comments'];
        $query = RebuyModel::InspectSubmit($order_id, $devicetype, $devicestorage, $deviceconnection, $devicecondition, $devicecolour, $devicecomments,$date,$IMEI);
        $status=RebuyModel::updateFStatus(16,$order_id);
        $action=RebuyModel::setAction(7,$order_id);
        $event=EventModel::createHistoryEvent($IMEI,1);

        $failcard=$_POST['failcard'];
        if (!isset($_POST['failcard'])) {


            echo "no parts require  replacement";

        } else {
            echo '<pre>';
            //var_dump($_POST['failcard']);
            // echo "See below for parts required";
            $failcard = $_POST['failcard'];
            echo '<pre>';
            echo '<h3>Parts Needed </h3></br>';
            foreach ($failcard as $value) {
                echo $value . "</br>";
            }


        }

        Return new Response($html);



    }

    /**
     * @Route("/rebuy/status")
     */
    public function status()
    {
        $results = RebuyModel::getStatus();
        $html=$this->render('Rebuy/status.html.twig'
            , [
            'results' => $results
        ]
        );
        Return new Response($html);
    }


    /**
     * @Route("/rebuy/{order_id}/mailsent")
     */
    public function mailsent($order_id)
    {
        //$order_id=$this->activate();
        $html=$this->render('Rebuy/mailsent.html.twig',
            [
                'order_id'=>$order_id
            ]);

        if(isset($_POST['submit'])) {
            $name=$_POST['name'];
            $email=$_POST['email'];
            $subject="Offer from Forza";
            $quote=$_POST['msg'];
            $body="Thank you for sending us your device. 
          We would like to offer you \r\n $quote \r\n euro for it". 'To accept or refuse please visit <a href='.'"'.'http://forzaerp.local/rebuy/'.$order_id.'/'.'acceptquote'.'"'.'>This Link</a>';
            echo '<pre>';
            var_dump($_POST);

            $mail = new PHPMailer(TRUE);

            try {

                $mail->setFrom('simona.thrussell@forza-refurbished.nl', $name);
                $mail->addAddress($email, 'your name');
                $mail->Subject = $subject;
                $mail->Body = $body;

                /* SMTP parameters. */
                $mail->isSMTP();
                $mail->Host = 'smtp.office365.com';
                $mail->SMTPAuth = TRUE;
                $mail->SMTPSecure = 'tls';
                $mail->Username = 'simona.thrussell@forza-refurbished.nl';
                $mail->Password = 'DcadkA7h';
                $mail->Port = 587;

                /* Disable some SSL checks. */
                $mail->SMTPOptions = array(
                    'ssl' => array(
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true
                    )
                );

                /* Finally send the mail. */
                $mail->send();
                $status=RebuyModel::updateFStatus(9,$order_id);
                $action=RebuyModel::setAction(14,$order_id);
            }
            catch (Exception $e)
            {
                echo $e->errorMessage();
            }



        }
        else{

            echo "no input submitted";

        }
        Return new Response($html);
    }

    /**
     * @Route("/rebuy/checkorders")
     */
    public function checkorders()
    {
        $results = RebuyModel::getCheck();
        $html=$this->render('Rebuy/checkorders.html.twig'
            , [
            'results' => $results
        ]
        );
        Return new Response($html);


    }

    /**
     * @Route("/rebuy/{order_id}/check")
     */
    public function check($order_id)
    {
        //$order_id=$this->activate();
        $results = RebuyModel::getDevice($order_id);
        $html=$this->render('Rebuy/check.html.twig',
            [
                'order_id'=>$order_id
            ]);
        Return new Response($html);
    }

    /**
     * @Route("/rebuy/{order_id}/checksubmit")
     */
    public function checksubmit($order_id)
    {
        //$order_id = $this->activate();

        $results=RebuyModel::getActionButtons($order_id);
        $html=$this->render('Rebuy/checksubmit.html.twig'
            ,['results'=>$results,
        'order_id'=>$order_id]
        );
        echo '<pre>';
        var_dump($_POST['check']);
        if (isset($_POST['check'])) {
            if (empty($_POST['IMEI'])) {

                die("IMEI not present. Please go back and enter it.");


            } else {
                $validate = Device::validateIMEI($_POST['IMEI']);
                $IMEI = $_POST['IMEI'];

            }

            if (empty($_POST['check'])) {
                echo "No options were checked";


            } else {
                $check = $_POST['check'];


                $N = count($check);
                if ($N == 3) {
                    echo "check passed!";
                    $checked = 1;
                    $date = date('Y-m-d');
                    $query = RebuyModel::checksubmit($IMEI, $checked, $order_id,$date);
                    $status = RebuyModel::updateFStatus(4, $order_id);
                    $action = RebuyModel::setAction(6, $order_id);
                    echo "Event id ".$event=EventModel::createHistoryEvent($IMEI,2). "has been created";
                    $_SESSION['imei']=$IMEI;


                } else {
                    echo "the device did not pass check, please see notes.";
                    $checked = 0;
                    $date = date('Y-m-d');
                    $query = RebuyModel::checksubmit($IMEI, $checked, $order_id,$date);
                    $status = RebuyModel::updateFStatus(5, $order_id);
                    $action = RebuyModel::setAction(4, $order_id);


                }
            }


        }else{

            echo "no data was submitted";
        }
        Return new Response($html);

    }

    /**
     * @Route("/rebuy/reports")
     */
    public function reports()
    {
        $html=$this->render('Rebuy/reports.html.twig'

        );
        Return new Response($html);

    }


    /**
     * @Route("rebuy/{order_id}/quote")
     */
    public function quote($order_id)
    {
        //$order_id=$this->activate();
        $results = RebuyModel::getDevice($order_id);
        $html=$this->render('Rebuy/quote.html.twig'
            , [
            'results' => $results,
                'order_id'=>$order_id
        ]
        );
        Return new Response($html);

    }

    /**
     * @Route("rebuy/quoteorders")
     */
    public function quoteorders()
    {
        $results = RebuyModel::getInspected();
        $html=$this->render('Rebuy/quoteorders.html.twig'
            , [
            'results' => $results
        ]
        );
        Return new Response($html);
    }

    /**
     * @Route("/rebuy/{order_id}/submitquote")
     */
    public function submitquote($order_id)
    {

        $quote = $_POST['quote'];
        $query = RebuyModel::SubmitQuote($order_id,$quote);
        $status=RebuyModel::updateFStatus(17,$order_id);
        $action=RebuyModel::setAction(8,$order_id);
        $results = RebuyModel::getQuote($order_id);
        $html=$this->render('Rebuy/submitquote.html.twig' ,
           [
               'results' => $results,
               'order_id'=>$order_id]
        );
        Return new Response($html);


    }

    /**
     * @Route("/rebuy/overview")
     */
    public function overview()
    {

        $results = RebuyModel::Overview();
        $html=$this->render('Rebuy/overview.html.twig'
            , [
                'results' => $results
            ]
        );
        Return new Response($html);

    }

    /**
     * @Route("/rebuy/enterorder")
     */
    public function enterorder()
    {

        $html=$this->render('Rebuy/enterorder.html.twig');

        Return new Response($html);


    }

    public function entersubmit()
    {

        $html=$this->render('Rebuy/entersubmit.html.twig');

        $devicetype = $_POST['device_type'];
        $devicestorage = $_POST['device_storage'];
        $deviceconnection = $_POST['device_connection'];
        $devicecondition = $_POST['device_condition'];
        $devicecolour = $_POST['device_colour'];

        $firstname=$_POST['first_name'];
        $lastname=$_POST['last_name'];
        $email=$_POST['email'];
        $phone=$_POST['phone'];
        $customer_type=$_POST['customer_type'];
        $postcode=$_POST['postcode'];
        $streetnumber=$_POST['street_number'];
        $addition=$_POST['addition'];
        $streetname=$_POST['street_name'];
        $city=$_POST['city'];
        $country=$_POST['country'];

        $paymenttype=$_POST['payment'];
        $IBAN=$_POST['IBAN'];
        $Tnv=$_POST['Tnv'];


        $customer_id= CustomerModel::createCustomer($firstname,$lastname,$email,$phone, $customer_type);
        echo '<pre>';
        echo 'Created ' .$customer_id;
        $query=CustomerModel::createAddress($customer_id,$postcode,$streetnumber,$addition,$streetname,$city,$country);
        echo RebuyModel::OrderSubmit($order_id, $devicetype, $devicestorage, $deviceconnection, $devicecondition, $devicecolour);
        echo $query;
        $order_id=OrderModel::createOrder($customer_id,$paymenttype);
        echo $order_id;
        //var_dump($_POST);
        echo  $device=DeviceModel::makeRebuyDevice($order_id,$devicetype,$devicestorage,$deviceconnection,$devicecondition,$devicecolour);
        echo $status=RebuyModel::makeStatus($order_id)."</br>";
        echo $shipping=RebuyModel::createShippingStatus($order_id);
        echo $payment=FinanceModel::createPaymentData($customer_id,$order_id,$IBAN,$Tnv);

        Return new Response($html);

    }
    /**
     * @Route("/rebuy/service")
     */
    public function service()
    {
        $html=$this->render('Rebuy/service.html.twig');
        Return new Response($html);

    }

    /**
     * @Route("/rebuy/{order_id}/acceptQuote")
     */
    public function acceptQuote($order_id)
    {
        $order_id=$this->activate();
        $_SESSION['order_id']=$order_id;
        $results=RebuyModel::getQuote($order_id);
        //echo '<pre>';
        print_r($results);
        $html=$this->render('Rebuy/acceptquote.html.twig'
            ,[
            'results'=>$results,
                'order_id'=>$order_id
        ]
        );
        Return new Response($html);





    }

    /**
     * @Route("/{order_id}/confirm")
     */
    public function confirm($order_id)
    {
        //$order_id=$this->activateAction();
        //$order_id=$_SESSION['order_id'];
        $results=RebuyModel::getQuote($order_id);
        //echo $order_id;
        $offer_type='first offer';
        $date = date('Y-m-d');

        $html=$this->render('Rebuy/confirm.html.twig');
        $offer=new Offer($results[0][21],$order_id,$results[0][14],$date,$offer_type,1);
         $update=RebuyModel::setAccepted($order_id,$date);
        $action=RebuyModel::setAction(9,$order_id);
        $fstatus=RebuyModel::updateFStatus(11,$order_id);
        $status=RebuyModel::updateCStatus(4,$order_id);
        Return new Response($html);


    }


    /**
     * @Route("/{order_id}/refuseoptions")
     */
    public function refuseoptions($order_id)
    {
        $order_id=$_SESSION['order_id'];
        $results=RebuyModel::getQuote($order_id);
        //echo $order_id;
        $offer_type='first offer';
        $date = date('Y-m-d');

        $offer=new Offer($results[0][21],$order_id,$results[0][14],$date,$offer_type,0);
        $html=$this->render('Rebuy/refuseoptions.html.twig',
            [
                'order_id'=>$order_id
            ]);
        Return new Response($html);

    }

    /**
     * @Route("/rebuy/addtags")
     */
    public function addtags()
    {

        $results = RebuyModel::Overview();
        $html=$this->render('Rebuy/addtags.html.twig'
            , [
                'results' => $results
            ]
        );
        Return new Response($html);


    }


    /**
     *
     */
    public function setStatus()
    {
        //$order_id=$this->activateAction();
        $results = RebuyModel::getStatus();

        foreach ($results as $result) {
            $order_id = $result['order_id'];
            $cstatus = $result['customer_order_status'];
            $fstatus = $result['status_id'];
            $action = $result['next_action_id'];
            $update=self::setAction($cstatus,$fstatus);



        }
        $html=$this->render('Rebuy/setstatus.html.twig'
            , [
                'results' => $results
            ]
        );
        Return new Response($html);
    }

    public function set($cstatus,$fstatus)
    {


        //$action;
        $status = RebuyModel::setAction($action, $order_id);
    }


    public function setQuote()
    {




    }


    /**
     * @Route("rebuy/devicereceived")
     */
    public function devicereceived()
    {
        $results = RebuyModel::Shipping();
        $html=$this->render('Rebuy/devicereceived.html.twig'
            , [
                'results' => $results
            ]
        );
        Return new Response($html);


    }

    /**
     * @Route("/rebuy/{order_id}/orderedit")
     */

    public function orderedit($order_id)
    {
        //$order_id=$this->activate();
        $results = RebuyModel::GetOrders();
        $html=$this->render('Rebuy/orderedit.html.twig'
            , [
                'results' => $results,
                'order_id'=>$order_id
            ]
        );
        Return new Response($html);

    }


    /**
     * @Route("/rebuy/shipping")
     */
    public function shipping()
    {
        $results = RebuyModel::getShipping();
        $html=$this->render('Rebuy/shipping.html.twig'
            , [
                'results' => $results
            ]
        );
        Return new Response($html);

    }

    /**
     * @Route("/rebuy/{order_id}/editshipping")
     */

    public function editshipping($order_id)
    {
        //$order_id=$this->activate();
        //$_SESSION['order_id']=$order_id;
        $results = RebuyModel::getShippingById($order_id);
        $html=$this->render('Rebuy/editshipping.html.twig'
            , [
                'results' => $results,
                'order_id'=>$order_id
            ]
        );
        Return new Response($html);

    }


    /**
     * @Route("/rebuy/{order_id/shippingupdate")
     */
    public function shippingupdate($order_id)
    {
        //$order_id=$this->activateAction();
        //$order_id=$_SESSION['order_id'];
        if(isset($_POST['submit'])) {
            //var_dump($_POST['submit']);
            $status = $_POST['status'];
            $query=RebuyModel::updateShipping($status, $order_id);

            switch($status) {
                case 3:
                    $fstatus = 3;
                    $query = RebuyModel::updateFStatus(3, $order_id);
                    $update= RebuyModel::setAction(2, $order_id);
                    break;

                case 7:
                    $action = 1;
                    $query = RebuyModel::updateFStatus(7, $order_id);
                    $query2 = RebuyModel::setAction(1, $order_id);
                    break;

            }

        }
        else{

            echo "no info was sent";
        }
       // $results = RebuyModel::getShippingById($order_id);
        $html=$this->render('Rebuy/shippingupdate.html.twig'
            , [
                'results' => $results,
                'order_id'=>$order_id
            ]
        );

        Return new Response($html);
    }

    /**
     * @Route("/rebuy/secondOffer")
     */
    public function secondOffer()
    {
        //$order_id=$this->activateAction();
        $results = RebuyModel::GetReturns();
        $html=$this->render('Rebuy/secondOffer.html.twig'
            , [
            'results' => $results
        ]
        );
        Return new Response($html);


    }


    /**
     * @Route("/rebuy/{order_id}/sebmitsecondquote")
     */
    public function submitsecondquote($order_id)
    {
        //$order_id=$this->activate();
        //$_SESSION['order_id'];
        $results=RebuyModel::retrievesecondquote($order_id);

        //$order_id=$_SESSION['order_id'];
        // var_dump($_POST);
        //$_SESSION['order_id']=$order_id;
        if(isset($_POST['submit'])) {
            $quote=$_POST['quote'];
            echo $query = RebuyModel::submitSecondQuote($order_id, $quote);
            $action=RebuyModel::setAction(10,$order_id);
        }else{

            echo "please enter new offer";
        };

        $html=$this->render('Rebuy/submitsecondquote.html.twig'
            ,[
            'results'=>$results,
                'order_id'=>$order_id

        ]
        );
        Return new Response($html);
    }

    /**
     * @Route("/rebuy/{order_id)/entersecondoffer")
     */
    public function entersecondoffer($order_id)
    {
        //$order_id=$this->activateAction();
        //$order_id=$_SESSION['order_id'];
        //$_SESSION['order_id']=$order_id;
        $results=RebuyModel::retrievefirstoffer($order_id);
        $html=$this->render('Rebuy/entersecondoffer.html.twig'
            ,[
            'results' => $results,
                'order_id'=>$order_id
        ]
        );
        Return new Response($html);


    }


    /**
     * @Route("/rebuy/{order_id}/sendsecoffer")
     */
    public function sendsecoffer($order_id)
    {

        $order_id=$_SESSION['order_id'];
        $results=RebuyModel::retrievesecondquote($order_id);
        $html=$this->render('Rebuy/sendsecoffer.html.twig'
            ,[
            'results' => $results,
                'order_id'=>$order_id
        ]
        );

        Return new Response($html);

    }

    public function ftstatusreports()
    {




    }


    /**
     * @Route("/rebuy/offers")
     */
    public function offers()
    {
        $results=RebuyModel::getOffers();
        $html=$this->render('Rebuy/offers.html.twig'
            ,['results' => $results
            ]
        );

        Return new Response($html);
    }


    /**
     * @Route("rebuy/payments")
     */
    public function payments()
    {
        $results=RebuyModel::getAcceptedOffers();
        $html=$this->render('Rebuy/payments.html.twig'
            ,[
                'results' => $results
            ]
        );
        Return new Response($html);


    }

    /**
     * @Route("rebuy/returns")
     */
    public function returns()
    {
        $results=RebuyModel::getReturns();
        $html=$this->render('rebuy/returns.html.twig'
            ,[
                'results' => $results
            ]
        );
        Return new Response($html);

    }

    /**
     * @Route("/rebuy/{order_id}/recycle")
     */

    public function recycle($order_id)
    {
        //$order_id=$_SESSION['order_id'];
        $html=$this->render('Rebuy/recycle.html.twig',
            [
                'order_id'=>$order_id
            ]);
        $status=RebuyModel::updateCStatus(6,$order_id);
        $fstatus=RebuyModel::updateFStatus(8,$order_id);
        $action=RebuyModel::setAction(12,$order_id);
        Return new Response($html);

    }

    /**
     * @Route("/rebuy/close")
     */
    public function close()
    {
        $results=RebuyModel::getClose();
        $html=$this->render('Rebuy/close.html.twig'
            ,[
                'results' => $results
            ]
        );
        Return new Response($html);


    }

    /**
     * @Route("/rebuy/{order_id}/closeorder")
     */
    public function closeorder($order_id)
    {
        //$order_id=$this->activate();
        $results=RebuyModel::getOrderToClose($order_id);
        $html=$this->render('Rebuy/closeorder.html.twig'
            ,[
            'results'=>$results,
            'order_id'=>$order_id

        ]
        );
        Return new Response($html);

    }

    /**
     * @Route("/rebuy/{order_id}/orderclose")
     */
    public function orderclose($order_id)
    {
        //$order_id=$this->activate();
        if(isset($_POST['submit']))
        {
            $result=RebuyModel::closeOrder($order_id);
        }
        $html=$this->render('Rebuy/orderclose.html.twig',
            [
                'order_id'=>$order_id
            ]);
        Return new Response($html);

    }

    /**
     * @Route("/rebuy/accepted")
     */
    public function accepted()
    {
        $html=$this->render('Rebuy/accepted.html.twig');
        Return new Response($html);

    }


    /**
     * @Route("/rebuy/{order_id}/pay")
     */
    public function pay($order_id)
    {
        //$order_id=$this->activate();
        $results=RebuyModel::getPaymentDetails($order_id);
        $html=$this->render('Rebuy/pay.html.twig'
            ,[
           'results'=>$results,
                'order_id'=>$order_id

        ]
        );
        Return new Response($html);

    }

    /**
     * @Route("/rebuy/{order_id}/send")
     */

    public function send($order_id)
    {
        //$order_id=$this->activate();
        $results=RebuyModel::getPaymentDetails($order_id);
        //echo '<pre>';

        $status=RebuyModel::updateFStatus(11,$order_id);
        $html=$this->render('Rebuy/send.html.twig'
            ,[
                'results'=>$results,
                'order_id'=>$order_id

            ]

        );
        // var_dump($results);
        Return new Response($html);

    }

    /**
     * @Route("/rebuy/refusedoffers")
     */
    public static function refusedoffers()
    {
        //$results=RebuyModel::getRefused1();
        $html=$this->render('Rebuy/refusedoffers.html.twig'
            ,[
                'results'=>$results

            ]
         );
        Return new Response($html);

    }

        /**
         * @Route("rebuy/recycleorders")
         */
    public function recycleorders()
    {
        $results=RebuyModel::getRecycle();

        $html=$this->render('Rebuy/recycleorders.html.twig'
            ,[
                'results' => $results,

            ]
        );
        Return new Response($html);

    }


    /**
     * @Route("rebuy/{order_id}/returndevice")
     */
    public function returndevice($order_id)
    {
        //$order_id=$_SESSION['order_id'];
        $html=$this->render('Rebuy/returndevice.html.twig',
            ['order_id'=>$order_id]);
        $status=RebuyModel::updateCStatus(12,$order_id);
        $action=RebuyModel::setAction(11,$order_id);
        Return new Response($html);

    }


    /**
     * @Route("/rebuy/{order_id}/sendsecondoffermail")
     */
    public function sendsecondoffermail($order_id)

    {
        //$order_id=$this->activate();
        $html=$this->render('Rebuy/sendsecondoffermail.html.twig',
            [
                'order_id'=>$order_id
            ]);

        if(isset($_POST['submit'])) {
            $name=$_POST['name'];
            $email=$_POST['email'];
            $subject="Second Offer from Forza";
            $quote=$_POST['msg'];
            $body="Thank you for sending us your device. 
                We would like to offer you \r\n $quote \r\n euro for it". 'To accept or refuse please visit <a href='.'"'.'http://forzaerp.local/rebuy/'.$order_id.'/'.'acceptsecquote'.'"'.'>This Link</a>';
            echo '<pre>';
            var_dump($_POST);

            $mail = new PHPMailer(TRUE);

            try {

                $mail->setFrom('simona.thrussell@forza-refurbished.nl', $name);
                $mail->addAddress($email, 'your name');
                $mail->Subject = $subject;
                $mail->Body = $body;

                /* SMTP parameters. */
               $mail->isSMTP();
                $mail->Host = 'smtp.office365.com';
                $mail->SMTPAuth = TRUE;
                $mail->SMTPSecure = 'tls';
                $mail->Username = 'simona.thrussell@forza-refurbished.nl';
                $mail->Password = 'DcadkA7h';
                $mail->Port = 587;

                /* Disable some SSL checks. */
                $mail->SMTPOptions = array(
                    'ssl' => array(
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true
                    )
                );

                /* Finally send the mail. */
                $mail->send();
                $status=RebuyModel::updateFStatus(10,$order_id);
                $action=RebuyModel::setAction(14,$order_id);
            }
            catch (Exception $e)
            {
                echo $e->errorMessage();
            }



        }
        else{

            echo "no input submitted";

        }
        Return new Response($html);

    }


    /**
     * @Route("/rebuy/{order_id}/sendsecmail")
     */
    public function sendsecmail($order_id)
    {
        $order_id=$_SESSION['order_id'];
        $results=RebuyModel::getSecQuote($order_id);

        $html=$this->render('Rebuy/sendsecmail.html.twig'
            ,['results'=>$results,
                'order_id'=>$order_id]
        );
        Return new Response($html);

    }

    /**
     * @Route("/{order_id}/acceptsecquote")
     */
    public function acceptsecquote($order_id)
    {
        //$order_id=$this->activate();
        //$_SESSION['order_id']=$order_id;
        $results=RebuyModel::retrievesecondquote($order_id);
        $html=$this->render('Rebuy/acceptsecquote.html.twig'
           ,[
            'results'=>$results,
                'order_id'=>$order_id
        ]
        );
        Return new Response($html);

    }

    /**
     * @Route("/{order_id}/secconfirm")
     */
    public function secconfirm($order_id)
    {
        //$order_id=$_SESSION['order_id'];
        $results=RebuyModel::getQuote($order_id);
        echo $order_id;
        $offer_type='second offer';
        $date = date('Y-m-d');


        $offer=new Offer($results[0][21],$order_id,$results[0][14],$date,$offer_type,1);
       $html=$this->render('Rebuy/secconfirm.html.twig',
           [
               'order_id'=>$order_id
           ]);
        //TO DO : check that second offer amount is entered
        $results=RebuyModel::getQuote($order_id);
        ;
       $action=RebuyModel::setAction(9,$order_id);
       $fstatus=RebuyModel::updateFStatus(11,$order_id);
        $status=RebuyModel::updateCStatus(7,$order_id);
        Return new Response($html);


    }


    /**
     * @Route("/rebuy/{order_id}/secrefuseoptions")
     */
    public function secrefuseoptions($order_id)
    {
        //$order_id=$_SESSION['order_id'];
        $results=RebuyModel::getQuote($order_id);
        echo $order_id;
        $offer_type='second offer';
        $date = date('Y-m-d');


       $offer=new Offer($results[0][21],$order_id,$results[0][14],$date,$offer_type,0);
        $html=$this->render('Rebuy/secrefuseoptions.html.twig',
            [
                'order_id'=>$order_id
            ]);
        Return new Response($html);

    }


    /**
     * @Route("/rebuy/{order_id}/return")
     */
    public function return($order_id)
    {
        $order_id=$this->activate();
        $results=RebuyModel::getAddressbyOrderId($order_id);
        $html=$this->render('Rebuy/return.html.twig'
            ,['results'=>$results,
            'order_id'=>$order_id]
        );
        Return new Response($html);

    }


    /**
     * @Route("/rebuy/{$order_id}/failcard")
     */
    public function failcard($order_id)
    {
        //$order_id=$this->activate();
        //$_SESSION['order_id']=$order_id;
        $html=$this->render('Rebuy/failcard.html.twig');
        Return new Response($html);
    }

    /**
     * @Route("/rebuy/{order_id}/setfailcard")
     */

    public function setfailcard($order_id)
    {


        //$order_id=$this->activate();
        //$results=RebuyModel::getOrderActionById($order_id);


       $result2=DeviceModel::getIMEI($order_id);
        $IMEI=$result2[0]['IMEI'] ;
        $html=$this->render('Rebuy/setfailcard.html.twig',['results'=>$results,
            'order_id'=>$order_id]);
      echo '<pre>';
        print_r($_POST);

        if(isset($_POST['submit']))
        {


            function IsChecked($chkname,$value)
            {
                if(!empty($_POST[$chkname]))
                {
                    foreach($_POST[$chkname] as $chkval)
                    {
                        if($chkval == $value)
                        {
                            return true;
                        }
                    }
                }
                return false;
            }


            if(IsChecked('failcard','battery'))
            {
                $battery=1;
            }else{
                $battery=0;
            }

            if(IsChecked('failcard','speakers'))
            {
                $speakers=1;
            }else{
                $speakers=0;
            }

            if(IsChecked('failcard','speakers'))
            {
                $lcd=1;
            }else{
                $lcd=0;
            }

            if(IsChecked('failcard','camera'))
            {
                $camera=1;
            }else{
                $camera=0;
            }

            if(IsChecked('failcard','microphone'))
            {
                $microphone=1;
            }else{
                $microphone=0;
            }

            if(IsChecked('failcard','powerbutton'))
            {
                $powerbutton=1;
            }else{
                $powerbutton=0;
            }

            echo $battery."</br>";
            echo $speakers."</br>";
            echo $lcd."</br>";
            echo $camera."</br>";
            echo $microphone."</br>";
            echo $powerbutton."</br>";



        }else{
            $failcard=0;
            $battery=0;
            $speakers=0;
            $lcd=0;
            $camera=0;
            $microphone=0;
            $powerbutton=0;



        }
        echo $fail=RebuyModel::setFailcard($order_id,$IMEI,$battery,$speakers,$lcd,$camera,$microphone,$powerbutton);

        Return new Response($html);

    }


    /**
     * @Route("/rebuy/{order_id}/takeaction")
     */
    public function takeaction($order_id)
    {

        $results=RebuyModel::getQuote($order_id);
        $html=$this->render('Rebuy/takeaction.html.twig'
            ,
            ['results'=>$results,
                'order_id'=>$order_id]

        );
        Return new Response($html);
    }

    /**
     * @Route("/rebuy/{order_id}/returnorder")
     */
    public function returnorder($order_id)
    {
        //$order_id=$this->activateAction();
        $results=RebuyModel::getQuote($order_id);
        $html=$this->render('Rebuy/returnorder.html.twig'
            ,
            ['results'=>$results,
                'order_id'=>$order_id]

        );
        Return new Response($html);

    }

    /**
     * @Route("/rebuy/{order_id}/recycledevice")
     */
    public function recycledevice($order_id)
    {
        //$order_id=$this->activate();
        $status=RebuyModel::updateFStatus(14,$order_id);
        $action=rebuyModel::setAction(13,$order_id);
        $results=RebuyModel::getOrderActionById($order_id);
        $html=$this->render('RebuyController/recycledevice.html.twig'
            ,
            ['results'=>$results,
                'order_id'=>$order_id]

        );
        Return new Response($html);

    }


}