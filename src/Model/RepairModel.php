<?php
/**
 * Created by PhpStorm.
 * User: SimonaThrussell
 * Date: 01/10/2018
 * Time: 09:14
 */

namespace App\Model;
use PDO;
use App\Core\Model;
class RepairModel extends Model
{


    public static function getFailcard($imei)
    {

            try {
                $db = static::getDB();
                $stmt = $db->prepare("SELECT * FROM forzaerp_rebuy_inspection_failcard
            WHERE device_IMEI=?");
                $stmt->execute([$imei]);
                $results = $stmt->fetchAll();
                return $results;
            } catch (\PDOException $e) {
                echo $e->getMessage();
            }


    }

    public static function makeRepair($device_imei,$date, $repair,$device_model,$repair_type)
    {
        try {
            $db = static::getDB();
            $sql = "INSERT INTO
            forzaerp_necessary_repair(`imei`,`date`,`repair`,`device_type_id`,`repair_type_id`)
            VALUES (?,?,?,?,?)";
            $stmt = $db->prepare($sql);
            $stmt->execute([$device_imei,$date, $repair,$device_model,$repair_type]);
            //$stmt = null;
            $message="Buttons/Jacks inspection details entered";
            return $message;

        } catch (\PDOException $e) {
            echo $e->getMessage();
        }


    }

    public static function getrepair2parts()
    {

        try {
            $db = static::getDB();
            $stmt = $db->prepare("SELECT * FROM forzaerp_repair2_parts_type
            WHERE device_id=?");
            $stmt->execute([1]);
            $results = $stmt->fetchAll();
            return $results;
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }




    }















}