<?php
/**
 * Created by PhpStorm.
 * UserController: SimonaThrussell
 * Date: 28/01/2019
 * Time: 16:27
 */

namespace App\Core;
use PDO;


abstract class Model
{
    protected static function getDB()
    {
        static $db=null;

        if($db===null)
        {
            $host='localhost';
            $dbname='magwork';
            $username='root';
            $password='';

            try {
                $dsn='mysql:host='.$host.';dbname='.$dbname;
                $db=new PDO($dsn,$username,$password);
                $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


            }catch(\PDOException $e){

                echo $e->getMessage();
            }
        }
        return $db;

    }
}