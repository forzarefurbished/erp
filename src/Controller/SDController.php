<?php
/**
 * Created by PhpStorm.
 * User: SimonaThrussell
 * Date: 30/01/2019
 * Time: 14:12
 */

namespace App\Controller;
use App\Model\RebuyModel;
use App\Model\RMAModel;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\CustomerSupportModel;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SDController extends AbstractController
{
    /**
     * @Route("/cs/index")
     */
    public function index()
    {

        $results = CustomerSupportModel::getOrders();
        $html=$this->render('CS/index.html.twig',
        ['results'=>$results]
        );
        Return new Response($html);

    }

    /**
     * @return Response
     * @Route("CS/RMAIntake")
     */
    public function RMAIntake()
    {

        $results = CustomerSupportModel::getOrders();
        $html=$this->render('CS/RMAIntake.html.twig',
        ['results'=>$results]
        );
        Return new Response($html);

    }

    /**
     * @return Response
     * @Route("CS/RebuyIntake")
     */
    public function RebuyIntake()
    {

        $results = CustomerSupportModel::getOrders();
        $html=$this->render('CS/RebuyIntake.html',
        ['results'=>$results]
        );
        Return new Response($html);

    }

    /**
     * @return Response
     * @Route("/CS/Shipping Status")
     */
    public function ShippingStatus()
    {

        $results = CustomerSupportModel::getOrders();
        $html=$this->render('CS/ShippingStatus.html.twig',
        ['results'=>$results]
        );
        Return new Response($html);

    }

    /**
     * @return Response
     * @Route("/CS/RebuyOrderStatus")
     */
    public function RebuyOrders()
    {

        $results = RebuyModel::getOrders();
        $html=$this->render('CS/RebuyOrders.html.twig',
        ['results'=>$results]
        );
        Return new Response($html);

    }

    /**
     * @return Response
     * @Route("/CS/RMAOrderStatus")
     */
    public function RMAOrders()
    {

        $results = RMAModel::getRMAs();
        $html=$this->render('CS/RMAOrders.html.twig',
        ['results'=>$results]
        );
        Return new Response($html);

    }

    /**
     * @return Response
     * @Route("/CS/checkIMEI")
     */
    public function checkImei()
    {

        $results = CustomerSupportModel::getOrders();
        $html=$this->render('CS/checkImei.html.twig',
        ['results'=>$results]
        );
        Return new Response($html);

    }

    /**
     * @return Response
     * @Route("/CS/Orderedit")
     */
    public function editorders()
    {
        $results = CustomerSupportModel::getOrders();
        $html=$this->render('CS/editorders.html.twig',
        ['results'=>$results]
        );
        Return new Response($html);
    }


    /**
     * @return Response
     * @Route("/CS/CustomerData")
     */
    public function CustomerData()
    {
        $results = CustomerSupportModel::getOrders();
        $html=$this->render('CS/CustomerData.html.twig',
        ['results'=>$results]
        );
        Return new Response($html);
    }


    /**
     * @Route("/CS/SaleOrders")
     */
    public function RetailOrders()
    {
        $results = CustomerSupportModel::getOrders();
        $html=$this->render('CS/RetailOrders.html.twig'
        ,['results'=>$results]
        );
        Return new Response($html);
    }


    /**
     * @return Response
     * @Route("/CS/SaleOrdersBusiness")
     */
    public function BusinessOrders()
    {
        $results = CustomerSupportModel::getOrders();
        $html=$this->render('CS/BusinessOrders.html.twig',
        ['results'=>$results]
        );
        Return new Response($html);
    }


    /**
     * @return Response
     * @Route("/CS/Reports")
     */
    public function Reports()
    {
        $results = CustomerSupportModel::getOrders();
        $html=$this->render('CS/reports.html.twig',
        ['results'=>$results]
        );
        Return new Response($html);
    }

    /**
     * @Route("/CS/checked")
     */
    public function checked()
    {
        $imei = $_POST['imei'];
        $_SESSION['imei']=$imei;
        $count=RMAModel::checkIMEI($imei);
        if($count!=0) {
            $results = RMAModel::getWarranty($imei);
            $html = $this->render('CS/checked.html.twig',
                ['results' => $results]
            );
        }else{
            $html = $this->render('CS/checkfailed.html.twig');
        }
        Return new Response($html);
    }

}