<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CustomerRepository")
 */
class Customer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $customer_first_name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $customer_last_name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $customer_email;

    /**
     * @ORM\Column(type="string", length=14)
     */
    private $customer_phone_no;

    /**
     * @ORM\Column(type="integer")
     */
    private $customer_type;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCustomerFirstName(): ?string
    {
        return $this->customer_first_name;
    }

    public function setCustomerFirstName(string $customer_first_name): self
    {
        $this->customer_first_name = $customer_first_name;

        return $this;
    }

    public function getCustomerLastName(): ?string
    {
        return $this->customer_last_name;
    }

    public function setCustomerLastName(string $customer_last_name): self
    {
        $this->customer_last_name = $customer_last_name;

        return $this;
    }

    public function getCustomerEmail(): ?string
    {
        return $this->customer_email;
    }

    public function setCustomerEmail(string $customer_email): self
    {
        $this->customer_email = $customer_email;

        return $this;
    }

    public function getCustomerPhoneNo(): ?string
    {
        return $this->customer_phone_no;
    }

    public function setCustomerPhoneNo(string $customer_phone_no): self
    {
        $this->customer_phone_no = $customer_phone_no;

        return $this;
    }

    public function getCustomerType(): ?int
    {
        return $this->customer_type;
    }

    public function setCustomerType(int $customer_type): self
    {
        $this->customer_type = $customer_type;

        return $this;
    }
}
