<?php
/**
 * Created by PhpStorm.
 * UserController: simona
 * Date: 24/09/2018
 * Time: 09:09
 */

namespace App\Controller;
use \Core\View;
namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Response;
use App\Model\RebuyModel;
use App\Model\DeviceModel;
use App\Model\EventModel;
use App\Model\OrderModel;
class HomeController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index()
   {
       $html=$this->render('Home/index.html',
       [

       ]
           );
       Return new Response($html);
   }

    /**
     * @Route("/home/login")
     */
       public function login()
    {
        $html=$this->render('Home/login.html.twig');
        Return new Response($html);
    }

    public function dologin()
    {
        $html=$this->render('Home/dologin.html');
        $username=$_POST['username'];
        $password=$_POST['password'];
        Return new Response($html);

    }



   protected function before()
    {
        //echo "(before) ";
        //return false;
    }

    protected function after()
    {
        //echo "(after) ";
    }

    /**
     * @return Response
     * @Route("/home/getlogin")
     */
    public function getlogin()
    {
        $html=$this->render('Home/getlogin.html.twig');
        Return new Response($html);
    }

    /**
     * @return Response
     * @Route("/home/sendrequest")
     */
    public function sendrequest()
    {
        if(isset($_POST))
        {
            var_dump($_POST);
        }
        $html=$this->render('Home/sendrequest.html.twig');
        Return new Response($html);
    }



}